## User study on Unit-testing


### Set up
Check out this git repository wit the command `git clone `. 
Create a own branch with the command `git checkout -b <your Name without brackets>`
After finishing the task commit your results witch `git commit -m "your commit message"` and push your branch to the upstream
`git push -u origin <your Name without brackets>`

#### Task
Your task is to implement the "hasWon" - Method in the TicTacToe class. The method is given the player-number (1 or 2) and should
return true when the given player has won the game. Otherwise it should return false. A player has won when he owns 3 places on the field in a row.
This could be horizontal, vertical or diagonal. The field "board" contains the placements the players had made. 0 means no Player has marked this field.
At the end write some unit Tests for your implementation in the File `src/test/java/TicTacToeTest.java` so that you think your implementation works. 
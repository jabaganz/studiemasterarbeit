package tictactoeStudie;

public class TicTacToe {

	int[][] board;

	public TicTacToe() {

		board = new int[3][3];

		// init board
		for (int i = 0; i < board[0].length; i++) {
			for (int j = 0; j < board.length; j++) {
				board[i][j] = 0;
			}
		}
	}

	public void set(int player, int x, int y) {
		if (x <= board.length - 1 && y <= board[0].length && x >= 0 && y >= 0 && board[x][y] == 0)
			board[x][y] = player;
	}

	public boolean hasWon(int player) {
		/*
		 * 
		 * implement functionality here
		 * 
		 */
		throw new RuntimeException("not yet implemented");
	}

	public int getFieldValue(int x, int y) {
		return board[x][y];
	}

	@Override
	public String toString() {
		String res = "-------------\n";
		for (int i = 0; i < 3; i++) {
			res += "| ";
			for (int j = 0; j < 3; j++) {
				res += (String.valueOf(board[i][j]) + " | ");
			}
			res += "\n";
		}
		res += "-------------";
		return res;
	}
}

package tictactoeStudie;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TicTacToeTest {

	TicTacToe t;

	@Before
	public void setUp() throws Exception {
		t = new TicTacToe();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void example_test() {
		
		int player = 2;
		int x_coord = 0;
		int y_coord = 1;
		
		t.set(player, x_coord, y_coord);
		assertThat(t.hasWon(1), is(false));
		assertThat(t.hasWon(2), is(false));
	}

	// add more tests here

}
